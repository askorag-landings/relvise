import pugPlugin from 'vite-plugin-pug'

/** @type {import('vite').UserConfig} */
export default {
  plugins: [pugPlugin({}, {
    getBEMClass(block) {
      return (element) => {
        return block + "__" + element
      }
    },
  })]
}
